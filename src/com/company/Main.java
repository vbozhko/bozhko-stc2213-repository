package com.company;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] arrayForShift = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        printArrayElements(arrayForShift);
        shiftNullableElement(arrayForShift);
        printArrayElements(arrayForShift);
        int index = returnIndex(arrayForShift, 15);
        System.out.println(index);
    }

    /**
     * Печатаем элементы массива в консоль
     * @param arrayForShift - массив для вывода на печать
     */
    private static void printArrayElements(int[] arrayForShift) {
        System.out.println(Arrays.toString(arrayForShift));
    }

    /**
     *
     * @param arrayForShift массив для поиска элементов
     * @param i - элемент, который ищем
     * @return индекс искомого числа
     */
    private static int returnIndex(int[] arrayForShift, int i) {
        int index = -1;
        for (int j = 0; j < arrayForShift.length; j++) {
            if (arrayForShift[j] == 15) {
                index = j;
            }
        }
        return index;
    }

    /**
     * @autor VictorBozhko
     * Процедура, которая перемещает все значимые элементы влево, заполнив нулевые
     * @param arrayForShift - массив для сортировки
     */
    private static void shiftNullableElement(int[] arrayForShift) {
        for (int i = 0; i < arrayForShift.length; i++) {
            if (arrayForShift[i] == 0) {
                for (int j = i +1; j < arrayForShift.length; j++) {
                    if (arrayForShift[j] != 0) {
                        arrayForShift[i] = arrayForShift[j];
                        arrayForShift[j] = 0;
                        break;
                    }
                }
            }
        }
    }
}
